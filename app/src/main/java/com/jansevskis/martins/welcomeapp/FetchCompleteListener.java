package com.jansevskis.martins.welcomeapp;

import java.util.ArrayList;

public interface FetchCompleteListener {
    void downloadComplete(ArrayList<Repository> repositories);
}